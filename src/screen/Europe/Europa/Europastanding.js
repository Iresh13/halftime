import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { WebView } from "react-native-webview";
import Icon from "react-native-vector-icons/AntDesign";

class Europastanding extends React.Component {
  goback = () => {
    this.props.navigation.goBack();
  };
  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={styles.toolbar}>
          <TouchableOpacity onPress={() => this.goback()}>
            <Icon name="caretleft" size={20} color="white" />
          </TouchableOpacity>
          <Text
            style={{
              color: "white",
              fontWeight: "bold",
              textAlign: "center"
            }}
          >
            Standings
          </Text>
          <View />
        </View>
        <WebView
          source={{
            uri: "https://www.uefa.com/uefaeuropaleague/season=2019/standings/"
          }}
          style={{ marginTop: 20 }}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  toolbar: {
    height: 60,
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: "black",
    elevation: 5
  }
});
export default Europastanding;
