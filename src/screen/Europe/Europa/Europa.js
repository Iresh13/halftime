import React from "react";
import { Text, View, StyleSheet, Image } from "react-native";
import { createAppContainer, createBottomTabNavigator } from "react-navigation";
import Europafixtures from "./Europafixtures";
import Europaresults from "./Europaresults";
import Europastanding from "./Europastanding";
import Europastats from "./Europastats";
import Entypo from "react-native-vector-icons/Entypo";

const Europa = createBottomTabNavigator(
  {
    Fixtures: Europafixtures,
    Latest: Europaresults,
    Standings: Europastanding,
    Stats: Europastats
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let IconComponent = Entypo;
        let iconName;
        if (routeName === "Fixtures") {
          iconName = `calendar`;
        } else if (routeName === "Latest") {
          iconName = `trophy`;
        } else if (routeName === "Standings") {
          iconName = `text`;
        } else if (routeName === "Stats") {
          iconName = `users`;
        } else if (routeName === "Teams") {
          iconName = `sports-club`;
        } else {
          iconName = "home";
        }

        return <IconComponent name={iconName} size={25} color={tintColor} />;
      }
    }),
    tabBarOptions: {
      activeTintColor: "tomato",
      inactiveTintColor: "gray",
      style: {
        height: 50,
        backgroundColor: "black"
      }
    }
  }
);

export default createAppContainer(Europa);
