import React from "react";
import {
  Text,
  View,
  StyleSheet,
  Platform,
  Image,
  SafeAreaView,
  FlatList,
  TouchableOpacity
} from "react-native";
import axios from "axios";
import moment from "moment";
import Toolbar from "../../navigation/Toolbar";

class App extends React.Component {
  constructor() {
    super();
    this.state = { articles: [], refreshing: false };
  }

  componentDidMount() {
    this.remoteCall();
  }

  remoteCall = () => {
    const url =
      "https://newsapi.org/v2/top-headlines?sources=football-italia&apiKey=fb3e0ed4f7e942aa8e1f37dc58519a50";
    axios
      .get(url)
      .then(res => {
        this.setState({ articles: res.data.articles, refreshing: false });
      })
      .catch(err => alert(err));
  };

  onRefresh = () => {
    this.setState({ refreshing: true }, () => {
      this.remoteCall();
    });
  };
  gotopage = (url, title) => {
    this.props.navigation.navigate("NewsDetail", {
      url: url,
      title: title
    });
  };
  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: "black" }}>
        <Toolbar navigation={this.props.navigation} />
        <FlatList
          refreshing={this.state.refreshing}
          onRefresh={() => this.onRefresh()}
          data={this.state.articles}
          renderItem={({ item }) => {
            return (
              <TouchableOpacity
                style={styles.card}
                onPress={() => this.gotopage(item.url, item.title)}
              >
                <View>
                  <Image
                    style={styles.image}
                    source={{ uri: item.urlToImage }}
                  />
                </View>
                <View style={styles.topView}>
                  <Text>{item.author}</Text>
                  <Text>{moment(item.publishedAt).fromNow()}</Text>
                </View>

                <Text style={styles.title}>{item.title}</Text>
                <Text style={styles.description}>{item.description} </Text>
              </TouchableOpacity>
            );
          }}
          keyExtractor={(item, index) => index.toString()}
        />
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  toolbar: {
    backgroundColor: "blue",
    width: "100%",
    height: 60,
    alignItems: "center",
    justifyContent: "center",
    elevation: 5
  },
  card: {
    margin: 5,
    backgroundColor: "#ffffff",
    borderWidth: 0.5,
    borderColor: "#ffffff",
    elevation: 3,
    padding: 5
  },

  image: {
    height: 200,
    width: "100%"
  },
  topView: {
    margin: 10,
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  title: {
    margin: 10,
    fontWeight: "bold",
    color: "black"
  },
  description: {
    margin: 10,
    fontWeight: "normal",
    color: "black"
  }
});

export default App;
