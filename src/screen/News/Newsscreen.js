import React from "react";

import {
  createDrawerNavigator,
  createAppContainer,
  createBottomTabNavigator
} from "react-navigation";
import Entypo from "react-native-vector-icons/Entypo";
import Bbc from "./Bbc";
import br from "./Br";
import footballitalia from "./FootballItalia";

const News = createBottomTabNavigator(
  {
    Bbc: Bbc,

    Br: br,
    FootballItalia: footballitalia
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let IconComponent = Entypo;
        let iconName;

        if (routeName === "Bbc") {
          (iconName = `newsletter`), (iconSize = 30);
        } else if (routeName === "Br") {
          iconName = `newsletter`;
          iconSize = 30;
        } else if (routeName === "FootballItalia") {
          iconName = `newsletter`;
          iconSize = 30;
        } else {
          iconName = "home";
        }

        return <IconComponent name={iconName} size={25} color={tintColor} />;
      }
    }),
    tabBarOptions: {
      activeTintColor: "tomato",
      inactiveTintColor: "gray",
      style: {
        height: 50,
        backgroundColor: "white"
      }
    }
  }
);

export default createAppContainer(News);
