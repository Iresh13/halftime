import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { WebView } from "react-native-webview";
import Icon from "react-native-vector-icons/AntDesign";

class NewsDetail extends React.Component {
  goback = () => {
    this.props.navigation.goBack();
  };
  render() {
    const url = this.props.navigation.getParam("url");
    const title = this.props.navigation.getParam("title");

    return (
      <View style={{ flex: 1 }}>
        <View style={styles.toolbar}>
          <TouchableOpacity onPress={() => this.goback()}>
            <Icon name="caretleft" size={20} color="white" />
          </TouchableOpacity>
          <Text style={{ color: "white", fontWeight: "bold" }}>{title}</Text>
        </View>
        <WebView source={{ uri: url }} style={{ marginTop: 20 }} />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  toolbar: {
    height: 60,
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: "blue",
    elevation: 5
  }
});
export default NewsDetail;
