import React from "react";
import { Text, View, StyleSheet, Image } from "react-native";
import { createAppContainer, createBottomTabNavigator } from "react-navigation";
import ligue1fixtures from "./ligue1fixtures";
import ligue1results from "./ligue1results";
import ligue1standing from "./ligue1standing";
import ligue1stats from "./ligue1stats";
import Ligue1Teams from "./team";
import Entypo from "react-native-vector-icons/Entypo";
const Ligue1 = createBottomTabNavigator(
  {
    Fixtures: ligue1fixtures,
    Results: ligue1results,
    Standings: ligue1standing,
    Stats: ligue1stats,
    Teams: Ligue1Teams
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let IconComponent = Entypo;
        let iconName;
        if (routeName === "Fixtures") {
          iconName = `calendar`;
        } else if (routeName === "Results") {
          iconName = `trophy`;
        } else if (routeName === "Standings") {
          iconName = `text`;
        } else if (routeName === "Stats") {
          iconName = `users`;
        } else if (routeName === "Teams") {
          iconName = `sports-club`;
        } else {
          iconName = "home";
        }

        return <IconComponent name={iconName} size={25} color={tintColor} />;
      }
    }),
    tabBarOptions: {
      activeTintColor: "tomato",
      inactiveTintColor: "gray",
      style: {
        height: 50,
        backgroundColor: "black"
      }
    }
  }
);

export default createAppContainer(Ligue1);
