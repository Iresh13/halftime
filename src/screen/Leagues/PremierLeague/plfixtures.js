import React from "react";
import {
  Text,
  StyleSheet,
  View,
  SafeAreaView,
  ScrollView,
  FlatList,
  Alert
} from "react-native";
import axios from "axios";
import SVGImage from "react-native-svg-image";
import moment, { version } from "moment";
import SToolbar from "../../../navigation/screenTool";
class Plfixtures extends React.Component {
  constructor() {
    super();
    this.state = { matches: [], logos: [] };
  }

  componentDidMount() {
    const url =
      "http://api.football-data.org/v2/competitions/2021/matches?status=SCHEDULED";
    const url2 = "http://api.football-data.org/v2/competitions/2021/standings";
    const headers = {
      "X-Auth-Token": "22d1216ce9ea41648e645cd3b31c13fe",
      Accept: "application/json"
    };
    axios
      .get(url, { headers })
      .then(res => {
        this.setState({ matches: res.data.matches });
      })
      .catch(err => {
        alert.err;
      });
    axios.get(url2, { headers }).then(res => {
      res.data.standings[0].table.map(item => {
        this.state.logos.push({ id: item.team.id, logo: item.team.crestUrl });
      });
    });
  }
  geturi = () => {
    const newlogo = this.state.logos.filter(items => {
      if (item.id === id) {
        return item;
      }
    });
    return newlogo[0].logo;
  };

  render() {
    console.log(this.state.logos);
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: "black" }}>
        <SToolbar navigation={this.props.navigation} />
        <ScrollView style={{ flex: 1 }}>
          <FlatList
            data={this.state.matches}
            renderItem={({ item }) => {
              return (
                <View style={[styles.topView, styles.border]}>
                  <View style={styles.first}>
                    <Text
                      style={{
                        fontSize: 9,
                        fontWeight: "bold",
                        fontStyle: "italic"
                      }}
                    >
                      {item.matchday ? item.matchday : ""}
                    </Text>
                  </View>

                  <View style={{ borderLeftWidth: 2 }}>
                    {this.state.logos.map(items => {
                      if (items.id === item.homeTeam.id) {
                        return (
                          <View style={{ padding: 10 }} key={items.logo}>
                            <SVGImage
                              source={{ uri: items.logo }}
                              style={{
                                height: 40,

                                flex: 2,
                                width: 40,
                                padding: 5
                              }}
                            />
                          </View>
                        );
                      }
                    })}
                  </View>
                  <View style={styles.fourth}>
                    <Text style={styles.title}>
                      {item.homeTeam.name ? item.homeTeam.name : ""}
                    </Text>
                  </View>
                  <View style={styles.third}>
                    <Text style={styles.title}>Vs</Text>
                    <View>
                      <Text style={styles.title} />
                    </View>
                    <View>
                      <Text style={styles.titles}>
                        {moment(item.utcDate).fromNow()}
                      </Text>
                    </View>
                  </View>

                  {this.state.logos.map(items => {
                    if (items.id === item.awayTeam.id) {
                      return (
                        <View style={{ padding: 10 }} key={items.logo}>
                          <SVGImage
                            source={{ uri: items.logo }}
                            style={{
                              height: 40,

                              flex: 2,
                              width: 40,
                              padding: 10
                            }}
                          />
                        </View>
                      );
                    }
                  })}
                  <View style={styles.fourth}>
                    <Text style={styles.title}>
                      {item.awayTeam.name ? item.awayTeam.name : ""}
                    </Text>
                  </View>
                </View>
              );
            }}
            keyExtractor={(item, index) => index.toString()}
          />
        </ScrollView>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  topView: {
    flex: 1,
    flexDirection: "row",
    marginTop: 3,
    backgroundColor: "white"
  },
  first: {
    width: 30,
    borderLeftWidth: 0.5,
    alignItems: "center",
    justifyContent: "center",
    padding: 5
  },
  second: {
    flex: 3,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    padding: 5
  },
  third: {
    width: 50,
    alignItems: "center",
    justifyContent: "center"
  },
  fourth: {
    flex: 3,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },

  topText: {
    fontWeight: "bold",
    fontStyle: "italic",
    fontSize: 20
  },
  border: {
    borderLeftWidth: 2,
    borderColor: "black",
    borderRightWidth: 2
  },
  title: {
    fontWeight: "bold",
    fontStyle: "italic",
    fontSize: 10,
    fontFamily: "Verdana"
  },
  titles: {
    fontWeight: "bold",
    fontStyle: "italic",
    fontSize: 8,
    fontFamily: "Verdana"
  }
});

export default Plfixtures;
