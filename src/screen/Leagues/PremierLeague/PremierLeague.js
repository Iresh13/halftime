import React from "react";
import { Text, View, StyleSheet, Image } from "react-native";
import { createAppContainer, createBottomTabNavigator } from "react-navigation";
import Plfixtures from "./plfixtures";
import Plresults from "./plresults";
import Plstanding from "./plstanding";
import Plstats from "./plstats";
import Teams from "./team";

import Entypo from "react-native-vector-icons/Entypo";

const PremierLeague = createBottomTabNavigator(
  {
    Fixtures: Plfixtures,
    Results: Plresults,
    Standings: Plstanding,
    Stats: Plstats,
    Teams: Teams
  },

  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let IconComponent = Entypo;
        let iconName;
        if (routeName === "Fixtures") {
          iconName = `calendar`;
        } else if (routeName === "Results") {
          iconName = `trophy`;
        } else if (routeName === "Standings") {
          iconName = `text`;
        } else if (routeName === "Stats") {
          iconName = `users`;
        } else if (routeName === "Teams") {
          iconName = `sports-club`;
        } else {
          iconName = "home";
        }

        return <IconComponent name={iconName} size={25} color={tintColor} />;
      }
    }),
    tabBarOptions: {
      activeTintColor: "tomato",
      inactiveTintColor: "gray",
      style: {
        height: 50,
        backgroundColor: "black"
      }
    }
  }
);

export default createAppContainer(PremierLeague);
