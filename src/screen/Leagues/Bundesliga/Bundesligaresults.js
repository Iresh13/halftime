import React from "react";
import {
  Text,
  StyleSheet,
  View,
  SafeAreaView,
  ScrollView,
  FlatList,
  Alert
} from "react-native";
import axios from "axios";
import moment from "moment";
import SVGImage from "react-native-svg-image";
import SToolbar from "../../../navigation/screenTool";

class Bundesligaresults extends React.Component {
  constructor() {
    super();
    this.state = { standing: [], logos: [] };
  }
  componentDidMount() {
    const url =
      "http://api.football-data.org/v2/competitions/2002/matches?status=FINISHED";
    const url2 = "http://api.football-data.org/v2/competitions/2002/standings";
    const headers = {
      "X-Auth-Token": "22d1216ce9ea41648e645cd3b31c13fe",
      Accept: "application/json"
    };
    axios
      .get(url, { headers })
      .then(res => {
        this.setState({ results: res.data.matches.reverse() });
      })
      .catch(err => {
        alert.err;
      });
    axios.get(url2, { headers }).then(res => {
      res.data.standings[0].table.map(item => {
        this.state.logos.push({ id: item.team.id, logo: item.team.crestUrl });
      });
    });
  }
  geturi = id => {
    const newlogo = this.state.logos.filter(item => {
      if (item.id === id) {
        return item;
      }
    });
    return newlogo[0].logo;
  };
  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: "black" }}>
        <SToolbar navigation={this.props.navigation} />
        <FlatList
          data={this.state.results}
          renderItem={({ item, index }) => {
            return (
              <View style={{ flex: 1 }}>
                {index === 0 ? (
                  <View style={[styles.topView, styles.border]}>
                    <Text style={[styles.firsts, styles.title, styles.border]}>
                      HOME TEAM
                    </Text>
                    <Text style={[styles.firsts, styles.title, styles.border]}>
                      SCORE
                    </Text>
                    <Text style={[styles.firsts, styles.title, styles.border]}>
                      AWAY TEAM
                    </Text>
                  </View>
                ) : null}

                <View style={[styles.table]}>
                  <View style={styles.firsts}>
                    <Text style={styles.text}>{item.homeTeam.name}</Text>

                    {this.state.logos.map(items => {
                      if (items.id === item.homeTeam.id) {
                        return (
                          <SVGImage
                            source={{ uri: items.logo }}
                            style={{
                              height: 80,
                              width: 80,
                              flex: 3,
                              padding: 10
                            }}
                            key={items.logo}
                          />
                        );
                      }
                    })}
                  </View>
                  <View style={[styles.firsts]}>
                    <Text style={styles.text2}>
                      {item.score.fullTime.homeTeam}
                    </Text>
                    <Text style={styles.text}>-</Text>
                    <Text style={styles.text2}>
                      {item.score.fullTime.awayTeam}
                    </Text>
                    <View />
                    <View>
                      <View>
                        <Text style={styles.title} />
                      </View>
                      <Text style={styles.text}>
                        {moment(item.lastUpdated).fromNow()}
                      </Text>
                      <View>
                        <Text style={styles.title} />
                      </View>
                    </View>

                    <View>
                      <Text style={styles.text}>
                        Match Day: {item.matchday}
                      </Text>
                    </View>
                  </View>
                  <View style={[styles.firsts]}>
                    <Text style={styles.text}>{item.awayTeam.name}</Text>

                    {this.state.logos.map(items => {
                      if (items.id === item.awayTeam.id) {
                        return (
                          <SVGImage
                            source={{ uri: items.logo }}
                            style={{
                              height: 80,
                              width: 80,
                              flex: 3,
                              padding: 10
                            }}
                            key={items.logo}
                          />
                        );
                      }
                    })}
                  </View>
                </View>
              </View>
            );
          }}
          keyExtractor={(item, index) => index.toString()}
        />
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  topView: {
    flex: 1,
    flexDirection: "row",
    padding: 5,
    backgroundColor: "tomato",
    marginTop: 3,
    alignItems: "center",
    justifyContent: "center"
  },
  table: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: "white",
    marginTop: 4
  },

  first: {
    flex: 1,
    width: 20,
    padding: 5
  },
  second: {
    flex: 4,
    flexDirection: "row",
    padding: 5
  },
  firsts: {
    flex: 1,
    width: 140,
    padding: 3,
    alignItems: "center"
  },

  title: {
    fontWeight: "bold",
    fontStyle: "italic",
    justifyContent: "center",

    color: "black",
    fontSize: 13
  },
  text: {
    fontWeight: "bold",
    fontStyle: "italic",

    color: "black",
    fontSize: 10,
    fontFamily: "Verdana"
  },
  text2: {
    fontWeight: "bold",
    fontStyle: "italic",
    color: "black",
    fontSize: 20,
    fontFamily: "Verdana"
  },
  border: {
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderTopWidth: 1
  }
});
export default Bundesligaresults;
