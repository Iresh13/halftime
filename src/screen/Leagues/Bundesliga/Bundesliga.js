import React from "react";
import { Text, View, StyleSheet, Image } from "react-native";
import { createAppContainer, createBottomTabNavigator } from "react-navigation";
import Bundesligafixtures from "./Bundesligafixtures";
import Bundesligaresults from "./Bundesligaresults";
import Bundesligastanding from "./Bundesligastanding";
import Bundesligastats from "./Bundesligastats";
import BundesligaTeams from "./team";
import Entypo from "react-native-vector-icons/Entypo";

const Bundesliga = createBottomTabNavigator(
  {
    Fixtures: Bundesligafixtures,
    Results: Bundesligaresults,
    Standings: Bundesligastanding,
    Stats: Bundesligastats,
    Teams: BundesligaTeams
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let IconComponent = Entypo;
        let iconName;
        if (routeName === "Fixtures") {
          iconName = `calendar`;
        } else if (routeName === "Results") {
          iconName = `trophy`;
        } else if (routeName === "Standings") {
          iconName = `text`;
        } else if (routeName === "Stats") {
          iconName = `users`;
        } else if (routeName === "Teams") {
          iconName = `sports-club`;
        } else {
          iconName = "home";
        }

        return <IconComponent name={iconName} size={25} color={tintColor} />;
      }
    }),
    tabBarOptions: {
      activeTintColor: "tomato",
      inactiveTintColor: "gray",
      style: {
        height: 50,
        backgroundColor: "black"
      }
    }
  }
);

export default createAppContainer(Bundesliga);
