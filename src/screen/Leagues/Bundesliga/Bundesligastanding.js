import React from "react";
import {
  Text,
  StyleSheet,
  View,
  SafeAreaView,
  ScrollView,
  FlatList,
  Alert
} from "react-native";
import axios from "axios";
import moment from "moment";
import SVGImage from "react-native-svg-image";
import SToolbar from "../../../navigation/screenTool";
class Bundesligastanding extends React.Component {
  constructor() {
    super();
    this.state = { standing: [] };
  }
  componentDidMount() {
    const url = "http://api.football-data.org/v2/competitions/2002/standings";
    const headers = {
      "X-Auth-Token": "22d1216ce9ea41648e645cd3b31c13fe",
      Accept: "application/json"
    };
    axios
      .get(url, { headers })
      .then(res => {
        // alert(JSON.stringify(res.data.standings));
        this.setState({ standing: res.data.standings });
      })
      .catch(err => {
        alert.err;
      });
  }
  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: "black" }}>
        <SToolbar navigation={this.props.navigation} />
        <ScrollView horizontal>
          <FlatList
            data={
              this.state.standing.length > 0 ? this.state.standing[0].table : []
            }
            renderItem={({ item, index }) => {
              return (
                <View style={{ flex: 1 }}>
                  {index === 0 ? (
                    <View style={[styles.topView, styles.border]}>
                      <Text style={[styles.first, styles.title, styles.border]}>
                        Pos
                      </Text>
                      <Text
                        style={[styles.second, styles.title, styles.border]}
                      >
                        Teams
                      </Text>
                      <Text
                        style={[styles.firsts, styles.title, styles.border]}
                      >
                        Played
                      </Text>
                      <Text
                        style={[styles.firsts, styles.title, styles.border]}
                      >
                        Won
                      </Text>
                      <Text
                        style={[styles.firsts, styles.title, styles.border]}
                      >
                        Draw
                      </Text>
                      <Text
                        style={[styles.firsts, styles.title, styles.border]}
                      >
                        Lost
                      </Text>
                      <Text
                        style={[styles.firsts, styles.title, styles.border]}
                      >
                        Goals For
                      </Text>
                      <Text
                        style={[styles.firsts, styles.title, styles.border]}
                      >
                        Goals Against
                      </Text>
                      <Text
                        style={[styles.firsts, styles.title, styles.border]}
                      >
                        Goals Difference
                      </Text>
                    </View>
                  ) : null}

                  <View style={[styles.table]}>
                    <View style={[styles.first, styles.border]}>
                      <Text style={styles.text}>{item.position}</Text>
                    </View>
                    <View style={[styles.second, styles.border]}>
                      <SVGImage
                        style={{ height: 50, width: 50 }}
                        source={{ uri: item.team.crestUrl }}
                      />

                      <Text style={styles.text}>{item.team.name}</Text>
                    </View>
                    <View style={[styles.firsts, styles.border]}>
                      <Text style={styles.text}>{item.points}</Text>
                    </View>
                    <View style={[styles.firsts, styles.border]}>
                      <Text style={styles.text}>{item.playedGames}</Text>
                    </View>
                    <View style={[styles.firsts, styles.border]}>
                      <Text style={styles.text}>{item.won}</Text>
                    </View>
                    <View style={[styles.firsts, styles.border]}>
                      <Text style={styles.text}>{item.draw}</Text>
                    </View>
                    <View style={[styles.firsts, styles.border]}>
                      <Text style={styles.text}>{item.lost}</Text>
                    </View>
                    <View style={[styles.firsts, styles.border]}>
                      <Text style={styles.text}>{item.goalsFor}</Text>
                    </View>
                    <View style={[styles.firsts, styles.border]}>
                      <Text style={styles.text}>{item.goalsAgainst}</Text>
                    </View>
                    <View style={[styles.firsts, styles.border]}>
                      <Text style={styles.text}>{item.goalDifference}</Text>
                    </View>
                  </View>
                </View>
              );
            }}
            keyExtractor={(item, index) => index.toString()}
          />
        </ScrollView>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  topView: {
    flex: 1,
    flexDirection: "row",
    padding: 5,
    backgroundColor: "red",
    marginTop: 3,
    alignItems: "center",
    justifyContent: "center"
  },
  table: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: "white",
    marginTop: 4
  },

  first: {
    flex: 1,
    width: 40,
    padding: 5
  },
  second: {
    flex: 4,
    flexDirection: "row",
    padding: 5,
    width: 270
  },
  firsts: {
    flex: 1,
    width: 100,
    padding: 5,
    alignItems: "center",
    justifyContent: "center"
  },

  title: {
    fontWeight: "bold",
    fontStyle: "italic",

    color: "white",
    fontSize: 13
  },
  text: {
    fontWeight: "bold",
    fontStyle: "italic",

    color: "black",
    fontSize: 10,
    fontFamily: "Verdana"
  },
  border: {
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderTopWidth: 1
  }
});

export default Bundesligastanding;
