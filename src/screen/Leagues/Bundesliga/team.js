import React from "react";
import {
  Text,
  StyleSheet,
  View,
  SafeAreaView,
  ScrollView,
  FlatList,
  Alert,
  Modal,
  Dimensions,
  TouchableOpacity
} from "react-native";
import axios from "axios";
import moment from "moment";
import SVGImage from "react-native-svg-image";
import SToolbar from "../../../navigation/screenTool";
import Entypo from "react-native-vector-icons/FontAwesome";
class BundesligaTeams extends React.Component {
  constructor() {
    super();
    this.state = { team: [], selectedItems: {}, visible: false };
  }
  getData = item => {
    this.setState({ selectedItems: item, visible: true });
  };
  componentDidMount() {
    const url = "http://api.football-data.org/v2/competitions/2002/teams";
    const headers = {
      "X-Auth-Token": "22d1216ce9ea41648e645cd3b31c13fe",
      Accept: "application/json"
    };
    axios
      .get(url, { headers })
      .then(res => {
        // alert(JSON.stringify(res.data.standings));
        this.setState({ team: res.data.teams });
      })
      .catch(err => {
        alert.err;
      });
  }

  render() {
    const selectedItems = this.state.selectedItems;
    const { height, width } = Dimensions.get("window");
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: "black" }}>
        <SToolbar navigation={this.props.navigation} />
        <FlatList
          data={this.state.team}
          renderItem={({ item, index }) => {
            return (
              <View style={{ flex: 1 }}>
                {index === 0 ? (
                  <View style={[styles.topView, styles.border]}>
                    <Text style={[styles.first, styles.title, styles.border]}>
                      Team
                    </Text>
                    <Text style={[styles.second, styles.title, styles.border]}>
                      Full Name
                    </Text>
                    <Text style={[styles.firsts, styles.title, styles.border]}>
                      Short Name
                    </Text>
                  </View>
                ) : null}
                <TouchableOpacity
                  style={[styles.table]}
                  onPress={() => this.getData(item)}
                >
                  <View style={[styles.first]}>
                    <Text style={styles.text}>{item.shortName}</Text>

                    <SVGImage
                      style={{ height: 40, width: 40 }}
                      source={{ uri: item.crestUrl }}
                    />
                  </View>
                  <View style={[styles.firsts]}>
                    <Text style={styles.text}>{item.name}</Text>
                  </View>
                  <View style={[styles.firsts]}>
                    <Text style={styles.text}>{item.tla}</Text>
                  </View>
                </TouchableOpacity>
              </View>
            );
          }}
          keyExtractor={(item, index) => index.toString()}
        />
        <Modal
          visible={this.state.visible}
          animationType={"slide"}
          onRequestClose={() => this.setState({ visible: false })}
          transparent
        >
          <View style={{ flex: 1, paddingTop: height * 0.5 }}>
            <View style={{ flex: 1, backgroundColor: "white" }}>
              <ScrollView>
                <View
                  style={{
                    height: 30,
                    marginRight: 10,
                    flexDirection: "row",
                    justifyContent: "flex-end"
                  }}
                >
                  <Entypo
                    name={"arrow-down"}
                    color="black"
                    onPress={() => {
                      this.setState({ visible: false });
                    }}
                  />
                </View>

                <View
                  style={{
                    flex: 2,
                    justifyContent: "center",
                    alignItems: "center",
                    marginVertical: 20
                  }}
                >
                  <Text style={styles.title2}>{selectedItems.name}</Text>
                  <SVGImage
                    source={{ uri: selectedItems.crestUrl }}
                    style={{ height: 100, width: 100, borderRadius: 50 }}
                  />
                </View>
                <View style={{ flex: 3 }}>
                  <Text style={styles.text2}>
                    {"Name    :   "}
                    <Text
                      style={{ fontWeight: "normal", fontFamily: "Verdana" }}
                    >
                      {selectedItems.name}
                    </Text>
                  </Text>
                  <Text style={styles.text2}>
                    {"Short Name    :   "}
                    <Text
                      style={{ fontWeight: "normal", fontFamily: "Verdana" }}
                    >
                      {selectedItems.shortName}
                    </Text>
                  </Text>
                  <Text style={styles.text2}>
                    {"Known As    :   "}
                    <Text
                      style={{ fontWeight: "normal", fontFamily: "Verdana" }}
                    >
                      {selectedItems.tla}
                    </Text>
                  </Text>
                  <Text style={styles.text2}>
                    {"Club Colors    :   "}
                    <Text
                      style={{ fontWeight: "normal", fontFamily: "Verdana" }}
                    >
                      {selectedItems.clubColors}
                    </Text>
                  </Text>
                  <Text style={styles.text2}>
                    {"Stadium    :   "}
                    <Text
                      style={{ fontWeight: "normal", fontFamily: "Verdana" }}
                    >
                      {selectedItems.venue}
                    </Text>
                  </Text>
                  <Text style={styles.text2}>
                    {"Founded Date    :   "}
                    <Text
                      style={{ fontWeight: "normal", fontFamily: "Verdana" }}
                    >
                      {selectedItems.founded}
                    </Text>
                  </Text>
                  <Text style={styles.text2}>
                    {"Address   :   "}
                    <Text
                      style={{ fontWeight: "normal", fontFamily: "Verdana" }}
                    >
                      {selectedItems.address}
                    </Text>
                  </Text>
                  <Text style={styles.text2}>
                    {"Contact    :   "}
                    <Text
                      style={{ fontWeight: "normal", fontFamily: "Verdana" }}
                    >
                      {selectedItems.phone}
                    </Text>
                  </Text>
                  <Text style={styles.text2}>
                    {"Email   :   "}
                    <Text
                      style={{ fontWeight: "normal", fontFamily: "Verdana" }}
                    >
                      {selectedItems.email}
                    </Text>
                  </Text>
                  <Text style={styles.text2}>
                    {"Website   :   "}
                    <Text
                      style={{ fontWeight: "normal", fontFamily: "Verdana" }}
                    >
                      {selectedItems.website}
                    </Text>
                  </Text>
                </View>
              </ScrollView>
            </View>
          </View>
        </Modal>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  topView: {
    flex: 1,
    flexDirection: "row",
    padding: 5,
    backgroundColor: "red",
    marginTop: 3,
    alignItems: "center",
    justifyContent: "center"
  },
  table: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: "white",
    marginTop: 4
  },

  first: {
    flex: 2,

    padding: 5
  },
  second: {
    flex: 1,
    flexDirection: "row",
    padding: 5
  },
  firsts: {
    flex: 1,

    padding: 5,
    alignItems: "center",
    justifyContent: "center"
  },

  title: {
    fontWeight: "bold",
    fontStyle: "italic",

    color: "white",
    fontSize: 13
  },
  title2: {
    fontWeight: "bold",
    fontStyle: "italic",

    color: "black",
    fontSize: 19
  },
  text: {
    fontWeight: "bold",
    fontStyle: "italic",

    color: "black",
    fontSize: 10,
    fontFamily: "Verdana"
  },
  text2: {
    fontWeight: "bold",
    fontStyle: "italic",
    marginHorizontal: 20,
    color: "black",
    fontSize: 15,
    fontFamily: "Verdana"
  },
  border: {
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderTopWidth: 1
  }
});

export default BundesligaTeams;
