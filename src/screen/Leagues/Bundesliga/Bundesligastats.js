import React from "react";
import {
  Text,
  StyleSheet,
  View,
  SafeAreaView,
  ScrollView,
  FlatList,
  Alert
} from "react-native";
import axios from "axios";
import moment from "moment";
import SVGImage from "react-native-svg-image";
import SToolbar from "../../../navigation/screenTool";
class Bundesligastats extends React.Component {
  constructor() {
    super();
    this.state = { scorers: [], logos: [] };
  }
  componentDidMount() {
    const url = "http://api.football-data.org/v2/competitions/2002/scorers";
    const url2 = "http://api.football-data.org/v2/competitions/2002/standings";
    const headers = {
      "X-Auth-Token": "22d1216ce9ea41648e645cd3b31c13fe",
      Accept: "application/json"
    };
    axios
      .get(url, { headers })
      .then(res => {
        // alert(JSON.stringify(res.data.scorers));
        this.setState({ scorers: res.data.scorers });
      })
      .catch(err => {
        alert.err;
      });
    axios.get(url2, { headers }).then(res => {
      res.data.standings[0].table.map(item => {
        this.state.logos.push({ id: item.team.id, logo: item.team.crestUrl });
      });
    });
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: "black" }}>
        <SToolbar navigation={this.props.navigation} />
        <ScrollView horizontal>
          <FlatList
            data={this.state.scorers}
            renderItem={({ item, index }) => {
              return (
                <View style={{ flex: 1 }}>
                  {index === 0 ? (
                    <View style={[styles.topView, styles.border]}>
                      <Text style={[styles.first, styles.title, styles.border]}>
                        Player
                      </Text>
                      <Text
                        style={[styles.second, styles.title, styles.border]}
                      >
                        Team
                      </Text>
                      <Text
                        style={[styles.firsts, styles.title, styles.border]}
                      >
                        Goals
                      </Text>
                      <Text
                        style={[styles.firsts, styles.title, styles.border]}
                      >
                        Shirt No.
                      </Text>
                      <Text
                        style={[styles.firsts, styles.title, styles.border]}
                      >
                        Position
                      </Text>
                      <Text
                        style={[styles.firsts, styles.title, styles.border]}
                      >
                        Nationality
                      </Text>
                      <Text
                        style={[styles.firsts, styles.title, styles.border]}
                      >
                        DOB
                      </Text>
                    </View>
                  ) : null}

                  <View style={[styles.table]}>
                    <View style={[styles.first, styles.border]}>
                      <Text style={styles.text}>
                        {item.player ? item.player.name : ""}
                      </Text>
                    </View>
                    <View style={[styles.second, styles.border]}>
                      {this.state.logos.map(items => {
                        if (items.id === item.team.id) {
                          return (
                            <SVGImage
                              source={{ uri: items.logo }}
                              style={{
                                height: 50,
                                width: 50
                              }}
                              key={items.logo}
                            />
                          );
                        }
                      })}
                    </View>
                    <View style={[styles.second, styles.border]}>
                      <Text style={styles.text}>{item.team.name}</Text>
                    </View>
                    <View style={[styles.firsts, styles.border]}>
                      <Text style={styles.text}>{item.numberOfGoals}</Text>
                    </View>
                    <View style={[styles.firsts, styles.border]}>
                      <Text style={styles.text}>{item.shirtNumber}</Text>
                    </View>
                    <View style={[styles.firsts, styles.border]}>
                      <Text style={styles.text}>
                        {item.player.position ? item.player.position : ""}
                      </Text>
                    </View>

                    <View style={[styles.firsts, styles.border]}>
                      <Text style={styles.text}>
                        {item.player.nationality ? item.player.nationality : ""}
                      </Text>
                    </View>
                    <View style={[styles.firsts, styles.border]}>
                      <Text style={styles.text}>
                        {item.player.dateOfBirth ? item.player.dateOfBirth : ""}
                      </Text>
                    </View>
                  </View>
                </View>
              );
            }}
            keyExtractor={(item, index) => index.toString()}
          />
        </ScrollView>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  topView: {
    flex: 1,
    flexDirection: "row",
    padding: 5,
    backgroundColor: "black",
    marginTop: 3,
    alignItems: "center",
    justifyContent: "center"
  },
  table: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: "white",
    marginTop: 2
  },

  first: {
    flex: 2,
    width: 100,
    padding: 5
  },
  second: {
    flex: 1,
    flexDirection: "row",
    padding: 5,
    justifyContent: "center",
    alignItems: "center"
  },
  firsts: {
    width: 100,
    flexDirection: "row",
    padding: 5,
    alignItems: "center",
    justifyContent: "center"
  },

  title: {
    fontWeight: "bold",
    fontStyle: "italic",

    color: "tomato",
    fontSize: 13
  },
  text: {
    fontWeight: "bold",
    fontStyle: "italic",

    color: "black",
    fontSize: 10,
    fontFamily: "Verdana"
  },
  border: {
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderTopWidth: 1,
    borderColor: "white"
  }
});

export default Bundesligastats;
