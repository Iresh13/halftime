import React from "react";
import { createStackNavigator, createAppContainer } from "react-navigation";
import TopTab from "./toptab";
import News from "../screen/News/Newsscreen";
import PremierLeague from "../screen/Leagues/PremierLeague/PremierLeague";
import Laliga from "../screen/Leagues/LaLiga/Laliga";
import Seriea from "../screen/Leagues/Serie A/Seriea";
import Primeiraliga from "../screen/Leagues/Primeira Liga/Primeiraliga";
import Ligue1 from "../screen/Leagues/Ligue1/ligue1";
import Uefa from "../screen/Europe/Uefa/Uefa";
import Europa from "../screen/Europe/Europa/Europa";
import Fifa from "../screen/Fifa/fifa";
import Ediversie from "../screen/Leagues/Ediversie/ediversie";
import Bundesliga from "../screen/Leagues/Bundesliga/Bundesliga";
import Plfixtures from "../screen/Leagues/PremierLeague/plfixtures";
import Plresults from "../screen/Leagues/PremierLeague/plresults";
import Plstanding from "../screen/Leagues/PremierLeague/plstanding";
import Teams from "../screen/Leagues/PremierLeague/team";
import Plstats from "../screen/Leagues/PremierLeague/plstats";
import Bundesligafixtures from "../screen/Leagues/Bundesliga/Bundesligafixtures";
import BundesligaTeams from "../screen/Leagues/Bundesliga/team";
import Bundesligaresults from "../screen/Leagues/Bundesliga/Bundesligaresults";
import Bundesligastanding from "../screen/Leagues/Bundesliga/Bundesligastanding";
import Bundesligastats from "../screen/Leagues/Bundesliga/Bundesligastats";
import Laligafixtures from "../screen/Leagues/LaLiga/Laligafixtures";
import LaligaTeams from "../screen/Leagues/LaLiga/team";
import Laligaresults from "../screen/Leagues/LaLiga/Laligaresults";
import Laligastanding from "../screen/Leagues/LaLiga/Laligastanding";
import Laligastats from "../screen/Leagues/LaLiga/Laligastats";
import Ligue1Teams from "../screen/Leagues/Ligue1/team";
import ligue1fixtures from "../screen/Leagues/Ligue1/ligue1fixtures";
import ligue1results from "../screen/Leagues/Ligue1/ligue1results";
import ligue1standing from "../screen/Leagues/Ligue1/ligue1standing";
import ligue1stats from "../screen/Leagues/Ligue1/ligue1stats";
import EdiversieTeams from "../screen/Leagues/Ediversie/team";
import Ediversiefixtures from "../screen/Leagues/Ediversie/ediversiefixtures";
import Ediversieresults from "../screen/Leagues/Ediversie/ediversieresults";
import Ediversiestanding from "../screen/Leagues/Ediversie/ediversiestanding";
import Ediversiestats from "../screen/Leagues/Ediversie/ediversiestats";
import PrimeiraligaTeams from "../screen/Leagues/Primeira Liga/team";
import Primeiraligafixtures from "../screen/Leagues/Primeira Liga/Primeiraligafixtures";
import Primeiraligaresults from "../screen/Leagues/Primeira Liga/Primeiraligaresults";
import Primeiraligastanding from "../screen/Leagues/Primeira Liga/Primeiraligastanding";
import Primeiraligastats from "../screen/Leagues/Primeira Liga/Primeiraligastats";
import SerieaTeams from "../screen/Leagues/Serie A/team";
import Serieafixtures from "../screen/Leagues/Serie A/Serieafixtures";
import Serieastanding from "../screen/Leagues/Serie A/Serieastanding";
import Seriearesults from "../screen/Leagues/Serie A/Seriearesults";
import Serieastats from "../screen/Leagues/Serie A/Serieastats";
import NewsDetail from "../screen/News/Newsdetail";
import Europafixtures from "../screen/Europe/Europa/Europafixtures";

const MainStack = createStackNavigator(
  {
    home: TopTab,
    News: News,
    PremierLeague: PremierLeague,
    Bundesliga: Bundesliga,
    Laliga: Laliga,
    SerieA: Seriea,
    PrimeiraLiga: Primeiraliga,
    Uefa: Uefa,
    Europa: Europa,
    Ligue1: Ligue1,
    Ediversie: Ediversie,
    Fifa: Fifa,
    BFixtures: Bundesligafixtures,
    BResults: Bundesligaresults,
    BStandings: Bundesligastanding,
    BStats: Bundesligastats,
    BTeams: BundesligaTeams,
    EFixtures: Ediversiefixtures,
    EResults: Ediversieresults,
    EStandings: Ediversiestanding,
    EStats: Ediversiestats,
    ETeams: EdiversieTeams,
    LFixtures: Laligafixtures,
    LResults: Laligaresults,
    LStandings: Laligastanding,
    LStats: Laligastats,
    LTeams: LaligaTeams,
    Fixtures: ligue1fixtures,
    Results: ligue1results,
    Standings: ligue1standing,
    Stats: ligue1stats,
    Teams: Ligue1Teams,
    PFixtures: Plfixtures,
    PResults: Plresults,
    PStandings: Plstanding,
    PStats: Plstats,
    PTeams: Teams,
    PPFixtures: Primeiraligafixtures,
    PPResults: Primeiraligaresults,
    PPStandings: Primeiraligastanding,
    PPStats: Primeiraligastats,
    PPTeams: PrimeiraligaTeams,
    SFixtures: Serieafixtures,
    SResults: Seriearesults,
    SStandings: Serieastanding,
    SStats: Serieastats,
    STeams: SerieaTeams,
    NewsDetail: NewsDetail,
    EuropaFixtures: Europafixtures
  },
  {
    initialRouteName: "home",
    headerMode: "none"
  }
);

export default createAppContainer(MainStack);
