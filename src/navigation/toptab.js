import React from "react";
import {
  createDrawerNavigator,
  createAppContainer,
  createMaterialTopTabNavigator
} from "react-navigation";

import News from "../screen/News/Newsscreen";

import CustomDrawerContentComponent from "./customview";

const TopTab = createDrawerNavigator(
  {
    News: {
      screen: News
    }
  },
  {
    contentComponent: CustomDrawerContentComponent
  }
);

export default createAppContainer(TopTab);
