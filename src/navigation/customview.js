import React from "react";
import {
  Text,
  StyleSheet,
  ScrollView,
  View,
  TouchableOpacity,
  ImageBackground,
  Image
} from "react-native";
import { DrawerItems, SafeAreaView } from "react-navigation";

import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import AntDesign from "react-native-vector-icons/AntDesign";

class CustomDrawerContentComponent extends React.Component {
  constructor() {
    super();
    this.state = { leagues: true };
    this.state = { europe: true };
    this.state = { fifa: true };
  }

  Leagues = () => {
    this.setState({ leagues: !this.state.leagues });
  };

  Europe = () => {
    this.setState({ europe: !this.state.europe });
  };

  Fifa = () => {
    this.setState({ fifa: !this.state.fifa });
  };

  render() {
    return (
      <ScrollView style={{ flex: 1, backgroundColor: "#B71C1C" }}>
        <SafeAreaView
          style={styles.container}
          forceInset={{ top: "always", horizontal: "never" }}
          transparent
        >
          <View style={styles.top}>
            <Image source={require("../../a.jpeg")} style={styles.image} />
            <Text style={styles.Title2}>Half Time</Text>
          </View>
          <View style={styles.middle}>
            <TouchableOpacity
              onPress={() => this.props.navigation.closeDrawer("News")}
              style={{ flex: 1 }}
            >
              <View style={{ flex: 1, flexDirection: "row", padding: 10 }}>
                <FontAwesome name={"newspaper-o"} size={20} color={"white"} />
                <Text style={styles.Title}>News</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => this.Leagues()}
              style={{ flex: 1 }}
            >
              <View style={{ flex: 1, flexDirection: "row", padding: 10 }}>
                <MaterialCommunityIcons
                  name={"soccer-field"}
                  size={20}
                  color={"white"}
                />
                <Text style={styles.Title}>Leagues</Text>
              </View>
            </TouchableOpacity>

            <View style={{ height: this.state.leagues ? 0 : "auto" }}>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate("PremierLeague");
                  this.setState({ leagues: true });
                  this.props.navigation.closeDrawer();
                }}
                style={{ flexDirection: "row", padding: 10 }}
              >
                <AntDesign name={"slack"} size={15} color={"white"} />
                <Text style={styles.text}>Premier League</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate("Laliga");
                  this.setState({ leagues: true });
                  this.props.navigation.closeDrawer();
                }}
                style={{ flexDirection: "row", padding: 10 }}
              >
                <AntDesign name={"slack"} size={15} color={"white"} />
                <Text style={styles.text}>La Liga</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate("Bundesliga");
                  this.setState({ leagues: true });
                  this.props.navigation.closeDrawer();
                }}
                style={{ flexDirection: "row", padding: 10 }}
              >
                <AntDesign name={"slack"} size={15} color={"white"} />
                <Text style={styles.text}>Bundesliga</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate("SerieA");
                  this.setState({ leagues: true });
                  this.props.navigation.closeDrawer();
                }}
                style={{ flexDirection: "row", padding: 10 }}
              >
                <AntDesign name={"slack"} size={15} color={"white"} />
                <Text style={styles.text}>Serie A</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate("Ligue1");
                  this.setState({ leagues: true });
                  this.props.navigation.closeDrawer();
                }}
                style={{ flexDirection: "row", padding: 10 }}
              >
                <AntDesign name={"slack"} size={15} color={"white"} />
                <Text style={styles.text}>Ligue 1</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate("Ediversie");
                  this.setState({ leagues: true });
                  this.props.navigation.closeDrawer();
                }}
                style={{ flexDirection: "row", padding: 10 }}
              >
                <AntDesign name={"slack"} size={15} color={"white"} />
                <Text style={styles.text}>Ediversie</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate("PrimeiraLiga");
                  this.setState({ leagues: true });
                  this.props.navigation.closeDrawer();
                }}
                style={{ flexDirection: "row", padding: 10 }}
              >
                <AntDesign name={"slack"} size={15} color={"white"} />
                <Text style={styles.text}>Primera Liga</Text>
              </TouchableOpacity>
            </View>

            <TouchableOpacity onPress={() => this.Europe()} style={{ flex: 1 }}>
              <View style={{ flex: 1, flexDirection: "row", padding: 10 }}>
                <AntDesign name={"earth"} size={20} color={"white"} />
                <Text style={styles.Title}>Europe</Text>
              </View>
            </TouchableOpacity>

            <View style={{ height: this.state.europe ? 0 : "auto" }}>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate("Uefa");
                  this.setState({ europe: true });
                  this.props.navigation.closeDrawer();
                }}
                style={{ flexDirection: "row", padding: 10 }}
              >
                <AntDesign name={"antdesign"} size={15} color={"white"} />
                <Text style={styles.text}>Uefa Champions League</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate("Europa");
                  this.setState({ europe: true });
                  this.props.navigation.closeDrawer();
                }}
                style={{ flexDirection: "row", padding: 10 }}
              >
                <AntDesign name={"antdesign"} size={15} color={"white"} />
                <Text style={styles.text}>Europa League</Text>
              </TouchableOpacity>
            </View>

            <View style={{ flex: 1 }}>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("Fifa")}
                style={{ flex: 1 }}
              >
                <View style={{ flex: 1, flexDirection: "row", padding: 10 }}>
                  <MaterialCommunityIcons
                    name={"soccer"}
                    size={20}
                    color={"white"}
                  />
                  <Text style={styles.Title}>Fifa</Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={styles.bottom} />
          </View>
        </SafeAreaView>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  top: {
    flex: 2,
    backgroundColor: "#B00020",
    padding: 10,
    marginBottom: 10,
    justifyContent: "center",
    alignItems: "center"
  },
  bottom: {
    flex: 1,
    backgroundColor: "rgba(0,0,0,0.9)"
  },
  middle: {
    flex: 4,
    backgroundColor: "#B71C1C"
  },
  text: {
    color: "white",
    marginLeft: 20,
    lineHeight: 20,
    fontSize: 16,
    fontStyle: "italic",

    fontFamily: "Verdana"
  },
  Title: {
    color: "white",

    fontSize: 20,
    marginHorizontal: 10,
    fontFamily: "Arial"
  },
  Title2: {
    color: "white",
    textAlign: "center",
    fontSize: 50,
    fontStyle: "italic",
    fontWeight: "bold",
    fontFamily: "Party LET"
  },
  image: {
    height: 100,
    width: 100,
    borderRadius: 50
  }
});
export default CustomDrawerContentComponent;
