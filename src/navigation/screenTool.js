import React from "react";
import {
  Text,
  View,
  StyleSheet,
  Platform,
  Image,
  SafeAreaView,
  FlatList,
  TouchableOpacity
} from "react-native";

import Entypo from "react-native-vector-icons/Ionicons";

class SToolbar extends React.Component {
  render() {
    return (
      <View style={styles.toolbar}>
        <Entypo
          name={"ios-arrow-back"}
          size={20}
          color={"white"}
          onPress={() => this.props.navigation.popToTop()}
        />

        <Text style={styles.title}>HALF TIME</Text>
        <View />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  toolbar: {
    backgroundColor: "black",
    width: "100%",
    height: 60,
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 10,
    flexDirection: "row",
    elevation: 5
  },
  title: {
    margin: 10,
    fontWeight: "bold",
    textAlign: "center",
    color: "tomato"
  }
});
export default SToolbar;
