import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, SafeAreaView } from "react-native";
import MainStack from "./src/navigation/MainStack";
import Toptab from "./src/navigation/toptab";
import News from "./src/navigation/toptab";

class App extends React.Component {
  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: "white" }}>
        <MainStack />
      </SafeAreaView>
    );
  }
}

export default App;
